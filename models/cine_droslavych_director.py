from odoo import models, fields
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

class CineDroslavychDirector(models.Model):
  _name = 'cine_droslavych.director'
  name = fields.Char(compute='_get_name', string="Nom complet", readonly='True', store=False, default='')
  nom = fields.Char(string='Nom', size=150, required=True)
  cognom = fields.Char(string='Cognom', size=150, required=True)
  datanaix = fields.Date(string='Data naix.')
  bibliografia = fields.Text(string='Bibliografia')
  foto = fields.Binary(string='Foto')
  edat = fields.Char(compute='_get_edat', string="Edat", readonly='True', store=False, default="")

  pelicula_id = fields.One2many('cine_droslavych.pelicula', 'director_id', string='Pel·lícules')

  def _get_name(self):
    for record in self:
      record.name = record.nom + " " + record.cognom

  def _get_edat(self):
    for record in self:
      if record.datanaix and record.datanaix <= fields.Date.today():
        record.edat = relativedelta(
          fields.Date.from_string(fields.Date.today()),
          fields.Date.from_string(record.datanaix)).years
      else:
        record.edat = ""