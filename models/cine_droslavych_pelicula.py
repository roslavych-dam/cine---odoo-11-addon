from odoo import models, fields
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

class CineDroslavychPelicula(models.Model):
  _name = 'cine_droslavych.pelicula'
  name = fields.Char(compute='_get_name', string="Títol complet", readonly='True', store=False, default='')
  titol = fields.Char(string='Títol', size=200, required=True)
  pais = fields.Char(string='País', size=200)
  any_estrena = fields.Integer(string='Any', required=True)
  duracio = fields.Integer(string='Duració', required=True)
  puntuacio = fields.Float(string='Puntuació')
  resum = fields.Text(string='Resum')
  foto = fields.Binary(string='Foto')
  antiguitat = fields.Char(compute='_get_antiguitat', string="Antiguitat", readonly='True', store=False, default="")
  
  genere_id = fields.Many2many('cine_droslavych.genere', string='Gènere')
  actor_id = fields.Many2many('cine_droslavych.actor', string='Actors')
  director_id = fields.Many2one('cine_droslavych.director', string='Director')

  def _get_name(self):
    for record in self:
      any_string = str(record.any_estrena)
      record.name = record.titol + " ("+ record.pais + "), " + str(record.any_estrena) + ""

  def _get_antiguitat(self):
    year_now = datetime.now().year
    for record in self:
      if record.any_estrena and record.any_estrena <= year_now:
        record.antiguitat = year_now - record.any_estrena
      else:
        record.antiguitat = ""