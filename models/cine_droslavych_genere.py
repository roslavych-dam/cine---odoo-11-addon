from odoo import models, fields
class CineDroslavychGenere(models.Model):
  _name = 'cine_droslavych.genere'
  nom = fields.Char(string='Nom', size=150, required=True)
  pelicula_id = fields.Many2many('cine_droslavych.pelicula', string='Pel·lícules')