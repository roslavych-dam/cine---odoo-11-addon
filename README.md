*Disclaimer: this project is in Italian and Catalan*

This is a small Odoo addon for managing movies. It includes tree view, kanban 
view and detail view of directors, actors, movies and genres.

It also includes a filter for the movies (categories Bad, Goog and Very Good, 
based on a movie score) and it includes i18n translations.

Relations:

![](images/cine-relations.jpg)

Actors:
![](images/actor-detail.jpg)
![](images/actor-kanban.jpg)

Directors:
![](images/director-kanban.jpg)

Movies:
![](images/peli-detail.jpg)
![](images/peli-kanban.jpg)

Movies filter:
![](images/filter.jpg)

Genres:
![](images/genere-tree.jpg)